using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AccelerometerTest : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textMesh;

    // Update is called once per frame
    void Update()
    {
        Vector3 acc = Input.acceleration;
        _textMesh.text = $"ACC: {acc}";
    }

}
