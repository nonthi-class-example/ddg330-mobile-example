using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SwipeTest : MonoBehaviour
{
    [SerializeField] private float _minDistance = 0.2f;
    [SerializeField] private float _maxTime = 1f;

    [SerializeField] private float _dotThreshold = 0.9f;

    [SerializeField] private TextMeshProUGUI _textMesh;

    private float _startTime;
    private Vector2 _startScreenPos;

    private float _endTime;
    private Vector2 _endScreenPos;

    private void OnEnable()
    {
        InputController.Instance.OnTouchDown += HandleTouchDown;
        InputController.Instance.OnTouchUp += HandleTouchUp;
    }

    private void OnDisable()
    {
        InputController.Instance.OnTouchDown -= HandleTouchDown;
        InputController.Instance.OnTouchUp -= HandleTouchUp;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        foreach (Touch aTouch in Input.touches)
        {
            if (aTouch.fingerId != 0) continue;

            if (aTouch.phase == TouchPhase.Began)
            {
                _startTime = Time.time;
                _startScreenPos = aTouch.position;
            }
            else if (aTouch.phase == TouchPhase.Ended)
            {
                _endTime = Time.time;
                _endScreenPos = aTouch.position;

                CheckSwipe();
            }
        }
        */
    }

    private void HandleTouchDown(Vector2 position)
    {
        _startTime = Time.time;
        _startScreenPos = position;
    }

    private void HandleTouchUp(Vector2 position)
    {
        _endTime = Time.time;
        _endScreenPos = position;

        CheckSwipe();
    }

    private void CheckSwipe()
    {
        float smallerSide = Mathf.Min(Screen.width, Screen.height);
        Vector2 percentageDist = (_endScreenPos - _startScreenPos) / smallerSide;

        if (_endTime - _startTime > _maxTime) return;
        if (percentageDist.magnitude < _minDistance) return;

        Vector2 normalizedSwipe = percentageDist.normalized;

        string result = "NULL";
        if (Vector2.Dot(normalizedSwipe, Vector2.up) >= _dotThreshold)
        {
            result = "UP";
        }
        else if (Vector2.Dot(normalizedSwipe, Vector2.down) >= _dotThreshold)
        {
            result = "DOWN";
        }
        else if(Vector2.Dot(normalizedSwipe, Vector2.left) >= _dotThreshold)
        {
            result = "LEFT";
        }
        else if(Vector2.Dot(normalizedSwipe, Vector2.right) >= _dotThreshold)
        {
            result = "RIGHT";
        }

        _textMesh.text = $"Swipe: {result}";
    }
}
