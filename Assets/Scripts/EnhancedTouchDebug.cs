using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedTouch = UnityEngine.InputSystem.EnhancedTouch;
using TMPro;

public class EnhancedTouchDebug : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _textMesh;

    private void OnEnable()
    {
        EnhancedTouch.EnhancedTouchSupport.Enable();
    }

    private void OnDisable()
    {
        EnhancedTouch.EnhancedTouchSupport.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        int touchCount = EnhancedTouch.Touch.activeTouches.Count;

        string debugText = "" + touchCount;
        foreach (EnhancedTouch.Touch touch in EnhancedTouch.Touch.activeTouches)
        {
            debugText += $"\n{touch.screenPosition}";
        }

        ShowText(debugText);
    }

    private void ShowText(string text)
    {
        _textMesh.text = text;
    }
}
