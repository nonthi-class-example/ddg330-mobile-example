using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;

public class InputController : Singleton<InputController>
{
    public delegate void TouchTapEvent(Vector2 position);
    public event TouchTapEvent OnTouchTap;

    public delegate void TouchDefaultEvent(Vector2 position);
    public event TouchDefaultEvent OnTouchDown;
    public event TouchDefaultEvent OnTouchUp;

    private PlayerInput _playerInput;

    protected override void Awake()
    {
        base.Awake();
        TryGetComponent(out _playerInput);
    }

    private void OnEnable()
    {
        _playerInput.actions["TouchTap"].performed += HandleTouchPressed;
        _playerInput.actions["TouchDefault"].performed += HandleTouchDown;
        _playerInput.actions["TouchDefault"].canceled += HandleTouchUp;
    }

    private void OnDisable()
    {
        _playerInput.actions["TouchTap"].performed -= HandleTouchPressed;
        _playerInput.actions["TouchDefault"].performed -= HandleTouchDown;
        _playerInput.actions["TouchDefault"].canceled -= HandleTouchUp;
    }

    private void HandleTouchPressed(InputAction.CallbackContext context)
    {
        OnTouchTap?.Invoke(GetPrimaryTouchPosition());
    }

    private void HandleTouchDown(InputAction.CallbackContext context)
    {
        OnTouchDown?.Invoke(GetPrimaryTouchPosition());
    }

    private void HandleTouchUp(InputAction.CallbackContext context)
    {
        OnTouchUp?.Invoke(GetPrimaryTouchPosition());
    }

    private Vector2 GetPrimaryTouchPosition()
    {
        return _playerInput.actions["TouchState"].ReadValue<TouchState>().position;
    }
}
