using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedTouch = UnityEngine.InputSystem.EnhancedTouch;
using TMPro;

public class PinchZoom : MonoBehaviour
{
    [SerializeField] private Camera _zoomCam;

    [SerializeField] private float _minOffset = -5f;
    [SerializeField] private float _maxOffset = -20f;

    [SerializeField] private float _zoomMult = 1f;

    [SerializeField] private TextMeshProUGUI _zoomTextMesh;

    private bool _zooming;
    private float _initialDistance;
    private float _initialZoom;

    // Update is called once per frame
    void Update()
    {
        if (EnhancedTouch.Touch.activeTouches.Count == 2)
        {
            if (!_zooming)
            {
                StartZoom();
            }
            else
            {
                float distanceDiffRatio = (GetCurrentTouchDistance() - _initialDistance) / Mathf.Min(Screen.width, Screen.height);
                SetZoom(_initialZoom + (distanceDiffRatio * _zoomMult));
            }
        }
        else
        {
            EndZoom();
        }

        _zoomTextMesh.text = $"{GetCurrentZoom()}";
    }

    private void StartZoom()
    {
        _zooming = true;
        _initialDistance = GetCurrentTouchDistance();
        _initialZoom = GetCurrentZoom();
    }

    private void EndZoom()
    {
        _zooming = false;
    }

    private void SetZoom(float zoomValue)
    {
        Vector3 camPos = _zoomCam.transform.position;
        camPos.z = Mathf.Lerp(_minOffset, _maxOffset, zoomValue);
        _zoomCam.transform.position = camPos;
    }

    private float GetCurrentTouchDistance()
    {
        EnhancedTouch.Touch primaryTouch = EnhancedTouch.Touch.activeTouches[0];
        EnhancedTouch.Touch secondaryTouch = EnhancedTouch.Touch.activeTouches[1];

        return Mathf.Max(Vector2.Distance(primaryTouch.screenPosition, secondaryTouch.screenPosition), 1f);
    }

    private float GetCurrentZoom()
    {
        return Mathf.InverseLerp(_minOffset, _maxOffset, _zoomCam.transform.position.z);
    }
}
