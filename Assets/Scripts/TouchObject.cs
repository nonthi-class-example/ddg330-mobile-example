using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchObject : MonoBehaviour
{
    [SerializeField] private Camera _cam;

    private Renderer _renderer;
    private RaycastHit _hit;

    private void Awake()
    {
        TryGetComponent(out _renderer);
    }

    private void OnEnable()
    {
        InputController.Instance.OnTouchTap += HandleTouch;
    }

    private void OnDisable()
    {
        InputController.Instance.OnTouchTap -= HandleTouch;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        foreach (Touch aTouch in Input.touches)
        {
            if (aTouch.phase == UnityEngine.TouchPhase.Began)
            {
                Ray ray = _cam.ScreenPointToRay(aTouch.position);

                if (Physics.Raycast(ray, out _hit))
                {
                    if (_hit.transform.gameObject == gameObject)
                    {
                        ChangeToRandomColor();
                    }
                }
            }
        }
        */
    }

    public void HandleTouch(Vector2 touchPosition)
    {
        Ray ray = _cam.ScreenPointToRay(touchPosition);

        if (Physics.Raycast(ray, out _hit))
        {
            if (_hit.transform.gameObject == gameObject)
            {
                ChangeToRandomColor();
            }
        }
    }

    private void ChangeToRandomColor()
    {
        _renderer.material.color = Random.ColorHSV(0f, 1f, 1f, 1f);
    }
}
